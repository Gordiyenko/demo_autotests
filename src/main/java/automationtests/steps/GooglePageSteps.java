package automationtests.steps;

import automationtests.reportable.pages.GooglePage;
import com.google.inject.Inject;
import io.qameta.allure.Step;
import org.testng.AssertJUnit;

public class GooglePageSteps {

    @Inject
    GooglePage page;

    @Step("Go to google main page")
    public GooglePageSteps openGoogleMainPage(){
        page.open();
        return this;
    }

    @Step("Do search by expression")
    public GooglePageSteps doSearch(String expression) {
        page.doSearch(expression);
        return this;
    }

    @Step("Search result status should contains expected substring")
    public GooglePageSteps checkResultStatus(String expSubstring){
        AssertJUnit.assertTrue(page.getSearchResultStatus().contains(expSubstring));
        return this;
    }

}
