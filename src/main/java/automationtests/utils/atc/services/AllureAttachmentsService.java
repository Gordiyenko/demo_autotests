package automationtests.utils.atc.services;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;

import java.io.BufferedReader;
import java.io.FileReader;

import static automationtests.utils.atc.services.configs.ConfigLoader.load;

public class AllureAttachmentsService extends BaseService {

    public static final String SCREEN_SHOT_PROP = "core.screen.shot";
    public static final String NO = "NO";
    public static final String ALWAYS = "ALWAYS";
    public static final String ON_FAILURE = "ON_FAILURE";
    private static String screenShotParam = load().coreScreenShot();

    public static void attScreenShot () {
        switch (screenShotParam) {
            case NO:{
                log().info("Didn't take screen shot. System property '" + SCREEN_SHOT_PROP + "' is '" + screenShotParam + "'");
                break;
            }
            case ALWAYS: {
              makeScreenShot(); break;
            }
        }
    }

    public static void attErrorScreenShot () {
        if (screenShotParam.equals(ALWAYS) || screenShotParam.equals(ON_FAILURE)) {
            makeErrorScreenShot();
        }
    }

    @Attachment(value = "Click here for file view")
    public static byte[] attFile(String fileName) {
        String outString = "", tmp;
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                while ((tmp = br.readLine()) != null) {
                    outString = outString + tmp + "\n";
                }
            }
            log().info("Successfully attached file '" + fileName + "'");
            return outString.getBytes();
        } catch (Exception e) {
            log().error("Error occurred during file '" + fileName + "' processing!");
            log().error(e);
            return ("Error occurred during file '" + fileName + "' processing!").getBytes();
        }
    }

    @Attachment(value = "Click here for txt view")
    public static byte[] attText(String text) {
        return text.getBytes();
    }

    @Attachment(value = "Click here for screen shot view")
    private static byte[] makeScreenShot () {
        log().info("'" + SCREEN_SHOT_PROP + "' is '" + screenShotParam + "'");
        return screenShot();
    }

    @Attachment(value = "Click here for ERROR screen shot view")
    private static byte[] makeErrorScreenShot () {
        return screenShot();
    }

    private static byte[] screenShot () {
        log().info("'" + SCREEN_SHOT_PROP + "' is '" + screenShotParam + "'");
        try {
            return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (TimeoutException ignore) {
            log().warn("Stopped TimeOutException during screen shot taken...");
            return ("Stopped TimeOutException during screen shot taken...").getBytes();
        } catch (Exception e) {
            log().error("Error occurred during screen shot taking! System property '" + SCREEN_SHOT_PROP + "' is '" + screenShotParam + "!'");
            log().error("INFO: " + e);
            return ("Error occurred during screen shot taking! System property '" + SCREEN_SHOT_PROP + "' is '" + screenShotParam + "'").getBytes();
        }
    }

}
