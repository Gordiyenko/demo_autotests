package automationtests.reportable.pages;

import org.testng.AssertJUnit;

public class CheckPage extends BasePage<CheckPage> {

    public void textShouldContains(String fullString, String expSubString){
        AssertJUnit.assertTrue(fullString.contains(expSubString));
    }

}
