package automationtests.utils.atc;

import automationtests.utils.atc.reporter.allure.AllureCustomAspect;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.inject.Inject;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import java.lang.reflect.Method;
import static automationtests.utils.atc.services.BaseService.log;

public abstract class ATCoreTest {

    @Inject
    protected ATCActions actions;

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method){
        SelenideLogger.addListener("allure", new AllureCustomAspect());
        log("thread '" + Thread.currentThread().getId() + "' " + method.getName() + " ");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext context){
        if (WebDriverRunner.hasWebDriverStarted()) {
            WebDriverRunner.getWebDriver().quit();
            log().info("Driver successfully terminated.");
        }
    }

    @AfterTest(alwaysRun = true)
    public void afterTest(ITestContext context) {
        if (WebDriverRunner.hasWebDriverStarted()) {
            WebDriverRunner.getWebDriver().quit();
            log().info("Driver successfully terminated.");
        }
    }

}
