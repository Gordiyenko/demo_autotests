package automationtests.utils.atc.exceptions;

public class TestBrokenError extends Error {

    public TestBrokenError(String msg){
        super(msg);
    }

    public TestBrokenError(String msg, Throwable t){
        super(msg, t);
    }

}
