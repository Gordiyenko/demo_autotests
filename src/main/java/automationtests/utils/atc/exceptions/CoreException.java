package automationtests.utils.atc.exceptions;

public class CoreException extends RuntimeException {

    public CoreException(){

    }

    public CoreException(String msg){
        super(msg);
    }

    public CoreException(Throwable t){
        super(t);
    }

}
