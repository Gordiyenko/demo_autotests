package automationtests.utils.atc.services;

import automationtests.utils.atc.exceptions.TestBrokenError;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.*;

import static automationtests.utils.atc.services.configs.ConfigLoader.load;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class WebDriverService extends BaseService {

    private final static Integer BROWSER_WIDTH = Integer.valueOf(System.getProperty("browser.width", "1920"));
    private final static Integer BROWSER_HEIGHT = Integer.valueOf(System.getProperty("browser.height", "1080"));
    public static final Integer WD_TIME_OUT = load().coreWebDriverTimeout();


    public void clickOn(By locator){
        try {
            $(locator).click();
            log().info("Click on '" + locator + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void setValue(By locator, String value){
        try {
            $(locator).setValue(value);
            log().info("Set value '" + value + "' into '" + locator + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitABit(int timeInMilliseconds) {
        try {
            Thread.sleep(timeInMilliseconds);
            log().info("Wait '" + timeInMilliseconds + "' using Thread.sleep");
        } catch (Exception e) {
            log().error("Error occurred during 'Thread.sleep' execution!");
        }
    }

    public void goTo(String url) {
        try {
            log().info("Go to URL '" + url + "'.");
            open(url);
            WebDriverRunner.getWebDriver().manage().window().setSize(new Dimension(BROWSER_WIDTH, BROWSER_HEIGHT));
            log().info("Successfully get to URL '" + WebDriverRunner.getWebDriver().getCurrentUrl() + "'.");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError("Can't get to URL '" + url + "'!", t);
        }
    }

    public void goTo(String url, String domain, String login, String pass) {
        try {
            log().info("Go to URL '" + url + "' with basic auth for domain '" + domain + "'.");
            open(url, domain, login, pass);
            WebDriverRunner.getWebDriver().manage().window().setSize(new Dimension(BROWSER_WIDTH, BROWSER_HEIGHT));
            log().info("Successfully get to URL '" + WebDriverRunner.getWebDriver().getCurrentUrl() + "'.");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError("Can't get to URL '" + url + "'!", t);
        }
    }

    public void executeJS(String js){
        try {
            Selenide.executeJavaScript(js);
            log().info("Successfully execute js: " + js);
        } catch (TimeoutException ignore){
            /*NOP*/
            log().warn("'Throwable' ignored during script execution '" +js + "'...");
        } catch (Throwable t) {
            log().warn("Error during js '" + js + "' execution!");
            log().warn("INFO: " + t);
            throw t;
        }
    }

    public void scroll(By locator, Integer xOffset, Integer yOffset){
        try {
            Selenide.actions().moveToElement($(locator), xOffset, yOffset).perform();
            log().info("Element '" + locator + "' scrolled X:'" + xOffset + "' Y:'" + yOffset + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void scroll(Integer xOffset, Integer yOffset){
        try {
            Selenide.executeJavaScript("window.scroll("+ xOffset + ", " + yOffset + ");");
            log().info("Scroll page by js X:'" + xOffset + "' Y:'" + yOffset + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void scroll(By locator){
        try {
            $(locator).scrollIntoView(true);
            log().info("Scroll '" + locator + "' into view");
        }catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void scroll(By locator, String s){
        try {
            $(locator).scrollIntoView(s);
            log().info("Scroll '" + locator + "' " + s);
        }catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileNotVisible(By locator) {
        try {
            $(locator).waitWhile(Condition.not(Condition.visible), WD_TIME_OUT);
            log().info("Wait while '" + locator + "' is not visible");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileNotVisible(By locator, Integer timeout) {
        try {
            $(locator).waitWhile(Condition.not(Condition.visible), timeout);
            log().info("Wait while '" + locator + "' is not visible");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileExist(By locator) {
        try {
            $(locator).waitWhile(Condition.exist, WD_TIME_OUT);
            log().info("Wait while '" + locator + "' exist");
        } catch (Throwable t) {
            log().error(t.getMessage());
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileNotExist(By locator) {
        try {
            $(locator).waitWhile(Condition.not(Condition.exist), WD_TIME_OUT);
            log().info("Wait while '" + locator + "' not exist");
        } catch (Throwable t) {
            log().error(t.getMessage());
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileVisible(By locator) {
        try {
            $(locator).waitWhile(Condition.visible, WD_TIME_OUT);
            log().info("Wait while '" + locator + "' is visible");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileCssClassNot(By locator, String expCssClass){
        try {
            $(locator).waitWhile(Condition.not(Condition.cssClass(expCssClass)), WD_TIME_OUT);
            log().info("Wait while '" + locator + "' css class wasn't equals '" + expCssClass + "'");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void waitWhileCssClass(By locator, String expCssClass){
        try {
            $(locator).waitWhile(Condition.cssClass(expCssClass), WD_TIME_OUT);
            log().info("Wait while '" + locator + "' css class wasn't equals '" + expCssClass + "'");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public String getAttribute(By locator, String attribute){
        try {
            String result = $(locator).getAttribute(attribute);
            log().info("Got '" + result + "' from attribute '" + attribute + "' from '" + locator + "'");
            return result;
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public String getTex(By locator){
        try {
            String result = $(locator).getText();
            log().info("Got text '" + result + "'from '" + locator + "'");
            return result;
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void setDriver(WebDriver driver){
        WebDriverRunner.setWebDriver(driver);
        log().info("Driver '" + driver + "' set.");
    }

    public void sendKeys(String sequence){
        try {
            Selenide.actions().sendKeys(sequence).perform();
            log().info("Sent char sequence '" + sequence + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void sendKeysAndEnter(String sequence){
        try {
            Selenide.actions().sendKeys(sequence).sendKeys(Keys.ENTER).perform();
            log().info("Sent char sequence '" + sequence + "' and ENTER");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void sendKeys(By locator, String sequence){
        try {
            $(locator).sendKeys(sequence);
            log().info("Sent char sequence '" + sequence + "' to element '" + locator + "'");
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public Boolean isVisible(By locator){
        try {
            Boolean result = $(locator).is(Condition.visible);
            log().info("Is element '" + locator + "' visible - '" + result + "'");
            return result;
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void switchToBrowserTab(int browserBookmarkIndex) {
        try {
            Selenide.switchTo().window(browserBookmarkIndex);
            log().info("Switched to browser '" + browserBookmarkIndex + "'");
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

    public void createNewBrowserTab() {
        this.switchToBrowserTab(0);
        try {
            Selenide.executeJavaScript("window.open(arguments[0])", new Object[0]);
        } catch (Throwable t) {
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

}
