package automationtests.reportable.pages;

import automationtests.utils.atc.ATCActions;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BasePage<T> extends ATCActions<BasePage> {

    public T waitWhileLoaderPresents(){
        waitWhileVisible(By.xpath("//div[@class='loader']"));
        return (T) this;
    }

    public T waitWhilePaymentPopUpPresents(){
        waitWhileExist(By.xpath("//div[@id='modal-content-11']"));
        return (T) this;
    }

    public T waitWhileGreenLoaderPresents(){
        waitWhileExist(By.xpath("//*[contains(@src,'green.svg')]"));
        return (T) this;
    }

    public WebDriver getDriver() {
        return WebDriverRunner.getWebDriver();
    }

}
