package demotests;

import automationtests.reportable.pages.CheckPage;
import automationtests.reportable.pages.GooglePage;
import automationtests.utils.atc.ATCoreTest;
import com.codeborne.selenide.WebDriverRunner;
import com.google.inject.Inject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import static automationtests.utils.atc.utils.internal.WebDriverFactory.chrome;

/*This class represents simple approach for  automation test realization - page object pattern. All logic of real web
page encapsulated into class GooglePage. All methods from GooglePage will be automatically logged into report. After
each action in GooglePage will be add a screenshot (can be modified from property file).*/

@Guice
public class DemoGoogleSearchTest extends ATCoreTest {

    @Inject
    GooglePage googlePage;

    @Inject
    CheckPage check;

    @BeforeClass
    public void init(){
        WebDriverRunner.setWebDriver(chrome());
    }

    @Test
    public void googleCheckSearchResult(){
        String actResult =
            googlePage
                .open()
                .doSearch("hello google")
                .getSearchResultStatus();

        check.textShouldContains(actResult, "sec");
    }

}
