package automationtests.utils.atc;

import automationtests.utils.atc.services.AllureAttachmentsService;
import automationtests.utils.atc.services.BaseService;
import automationtests.utils.atc.services.WebDriverService;
import com.google.inject.Inject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ATCActions<T> extends BaseService {

    @Inject
    WebDriverService wd;

    public T attachScreenShot(){
        AllureAttachmentsService.attScreenShot();
        return (T) this;
    }

    public T attachFile(String filePath){
        AllureAttachmentsService.attFile(filePath);
        return (T) this;
    }

    public T attachText(String text){
        AllureAttachmentsService.attText(text);
        return (T) this;
    }

    public T clickOn(By locator){
        wd.clickOn(locator);
        return (T) this;
    }

    public T setValue(By locator, String value){
        wd.setValue(locator,value);
        return (T) this;
    }

    public T goTo(String url){
        wd.goTo(url);
        return (T) this;
    }

    public T goTo(String url, String domain, String login, String pass){
        wd.goTo(url, domain, login, pass);
        return (T) this;
    }

    public T scroll(By locator, Integer xOffset, Integer yOffset){
        wd.scroll(locator, xOffset, yOffset);
        return (T) this;
    }

    public T scroll(Integer xOffset, Integer yOffset){
        wd.scroll(xOffset, yOffset);
        return (T) this;
    }

    public T scroll(By locator){
        wd.scroll(locator);
        return (T) this;
    }

    public T scroll(By locator, String s){
        wd.scroll(locator, s);
        return (T) this;
    }

    public T waitWhileNotVisible(By locator){
        wd.waitWhileNotVisible(locator);
        return (T) this;
    }

    public T waitWhileNotVisible(By locator, Integer timeout){
        wd.waitWhileNotVisible(locator, timeout);
        return (T) this;
    }

    public T waitWhileVisible(By locator){
        wd.waitWhileVisible(locator);
        return (T) this;
    }

    public T waitWhileExist(By locator){
        wd.waitWhileExist(locator);
        return (T) this;
    }

    public T waitWhileNotExist(By locator){
        wd.waitWhileNotExist(locator);
        return (T) this;
    }

    public T waitWhileCssClassNot(By locator, String expCssClass){
        wd.waitWhileCssClassNot(locator, expCssClass);
        return (T) this;
    }

    public T waitWhileCssClass(By locator, String expCssClass){
        wd.waitWhileCssClass(locator, expCssClass);
        return (T) this;
    }

    public String getAttribute(By locator, String atr){
        return wd.getAttribute(locator, atr);
    }

    public String getTex(By locator){
        return wd.getTex(locator);
    }

    public T waitABit(int timeInMilliseconds) {
        wd.waitABit(timeInMilliseconds);
        return (T) this;
    }

    public T setDriver(WebDriver driver){
        wd.setDriver(driver);
        return (T) this;
    }

    public T sandKeys(String sequence){
        wd.sendKeys(sequence);
        return (T) this;
    }

    public T sendKeysAndEnter(String sequence){
        wd.sendKeysAndEnter(sequence);
        return (T) this;
    }

    public T sandKeys(By locator, String sequence){
        wd.sendKeys(locator, sequence);
        return (T) this;
    }

    public T createBrowserTab(){
        wd.createNewBrowserTab();
        return (T) this;
    }

    public T switchToBrowserTab(int index){
        wd.switchToBrowserTab(index);
        return (T) this;
    }

    public Boolean isVisible(By locator){
        return wd.isVisible(locator);
    }

//    public String googleGetFileData(String fileId, String mimeType){
//        try {
//            String fileData = GoogleDriveAPIWrapper.getFileData(fileId, mimeType);
//            getContainer().log().info("Data from google drive file '" + fileId + "':\n" + fileData);
//            return fileData;
//        } catch (Throwable t){
//            getContainer().log().error("Failed get data from file '" + fileId + "' from google disk!", t);
//            throw new TestBrokenError("Failed get data from file '" + fileId + "' from google disk!", t);
//        }
//    }

}