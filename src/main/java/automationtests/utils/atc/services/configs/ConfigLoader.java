package automationtests.utils.atc.services.configs;

import org.aeonbits.owner.ConfigFactory;

public class ConfigLoader {

    private static Configuration conf;

    public static Configuration load(){
        if (conf == null){
            conf = ConfigFactory.create(Configuration.class, System.getProperties());
        }
        return conf;
    }

}
