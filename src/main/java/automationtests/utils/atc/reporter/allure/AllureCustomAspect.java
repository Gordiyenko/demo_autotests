package automationtests.utils.atc.reporter.allure;

import automationtests.utils.atc.services.AllureAttachmentsService;
import com.codeborne.selenide.logevents.LogEvent;
import com.codeborne.selenide.logevents.LogEventListener;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StatusDetails;
import io.qameta.allure.model.StepResult;
import io.qameta.allure.util.ResultsUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.qameta.allure.util.ResultsUtils.getStatus;
import static io.qameta.allure.util.ResultsUtils.getStatusDetails;

/*This class overrides default allure behaviour.*/
@Aspect
public class AllureCustomAspect implements LogEventListener {

    private final AllureLifecycle lifecycle;

    public AllureCustomAspect() {
        this(Allure.getLifecycle());
    }

    public AllureCustomAspect(final AllureLifecycle lifecycle) {
        this.lifecycle = lifecycle;
    }

    /*Pointcut for all methods in all classes in package 'automationtests.reportable.', except methods: getIndex() and
    * all methods started from 'new'. This exception needs to avoid 'NullPointerException' or new web driver starting
    * during initialize objects in package by Guice DI*/
    @Pointcut("execution(* automationtests.reportable.*.*.*(..))" +
            "&& !execution(* automationtests.reportable.*.*.getIndex(..))" +
                "&& !execution(* automationtests.reportable.*.*.newInst*(..))")  // -> (2)
    public void anyMethod() {
        //pointcut body, should be empty
    }

    /*This part is responsible for any 'public void' methods view in report */
    @Around("anyMethod()")
    public Object step(ProceedingJoinPoint joinPoint) throws Throwable {
        final MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        final String name = joinPoint.getArgs().length > 0
                ? String.format("%s (%s)", methodSignature.getName(), arrayToString(joinPoint.getArgs())) // -> (1)
                : methodSignature.getName();
        final String uuid = UUID.randomUUID().toString();
        final StepResult result = new StepResult()
                .withName(name);
        lifecycle.startStep(uuid, result);
        try {
            final Object proceed = joinPoint.proceed();
            lifecycle.updateStep(uuid, s -> s.withStatus(Status.PASSED));
            AllureAttachmentsService.attScreenShot();
            return proceed;
        } catch (Throwable e) {
            lifecycle.updateStep(uuid, s -> s
                    .withStatus(getStatus(e).orElse(Status.BROKEN))
                    .withStatusDetails(getStatusDetails(e).orElse(null)));
            AllureAttachmentsService.attErrorScreenShot();
            throw e;
        } finally {
            lifecycle.stopStep(uuid);
        }
    }

    /*This part is responsible for Selenide methods view in report */
    @Override
    public void onEvent(final LogEvent event) {
        lifecycle.getCurrentTestCase().ifPresent(uuid -> {
            final String stepUUID = UUID.randomUUID().toString();
            lifecycle.startStep(stepUUID, new StepResult()
                    .withName(event.toString())
                    .withStatus(Status.PASSED));

            lifecycle.updateStep(stepResult -> stepResult.setStart(stepResult.getStart() - event.getDuration()));

            if (LogEvent.EventStatus.FAIL.equals(event.getStatus())) {
                lifecycle.updateStep(stepResult -> {
                    final StatusDetails details = ResultsUtils.getStatusDetails(event.getError())
                            .orElse(new StatusDetails());
                    stepResult.setStatus(ResultsUtils.getStatus(event.getError()).orElse(Status.BROKEN));
                });
            }
            lifecycle.stopStep(stepUUID);
        });
    }

    private static String arrayToString(final Object... array) {
        return Stream.of(array)
                .map(object -> {
                    if (object.getClass().isArray()) {
                        return arrayToString((Object[]) object);
                    }
                    return Objects.toString(object);
                })
                .collect(Collectors.joining(", "));
    }

}