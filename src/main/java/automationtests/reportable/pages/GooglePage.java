package automationtests.reportable.pages;

import org.openqa.selenium.By;

public class GooglePage extends BasePage<GooglePage> {

    private static final String URL = "https://www.google.com/en";
    private static final By INPUT_SEARCH = By.xpath("//input[@name='q']/..");
    private static final By RESULT_STATUS = By.xpath("//*[@id='resultStats']");

    public GooglePage open(){
        goTo(URL).waitWhileNotVisible(INPUT_SEARCH);
        return this;
    }

    public GooglePage doSearch(String value){
        sendKeysAndEnter(value);
        return this;
    }

    public String getSearchResultStatus(){
        String result = getTex(RESULT_STATUS);
        attachText(result);
        return result;
    }

}
