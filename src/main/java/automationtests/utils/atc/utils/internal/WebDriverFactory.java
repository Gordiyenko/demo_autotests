package automationtests.utils.atc.utils.internal;

import automationtests.utils.atc.exceptions.TestBrokenError;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URI;

import static automationtests.utils.atc.services.BaseService.log;
import static automationtests.utils.atc.services.configs.ConfigLoader.load;

public class WebDriverFactory {

    private static final String SELENOID_HUB_URL = load().selenoidHubUrl();

    static {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    public static WebDriver chrome(){
        return new ChromeDriver();
    }

    public static WebDriver fireFox(){
        return new FirefoxDriver();
    }

    public static RemoteWebDriver chromeSelenoid() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setVersion("latest");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);

        try {
            return new RemoteWebDriver(
                    URI.create(SELENOID_HUB_URL).toURL(),
                    capabilities
            );
        } catch (Throwable t){
            log().error(t);
            throw new TestBrokenError(t.getMessage(), t);
        }
    }

}
