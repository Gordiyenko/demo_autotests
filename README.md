##### Requirements:
- java 8;
- maven 3.5 (or higher) 
- allure command line (https://bintray.com/qameta/generic/allure2/2.7.0)
- chrome 70

##### Test run command: `mvn clean test -f pom.xml`

##### Report generation: `allure serve .\target\allure-results\`

##### For viewing already generated report, please open index.html (any browser except chrome) from allure-report folder. 