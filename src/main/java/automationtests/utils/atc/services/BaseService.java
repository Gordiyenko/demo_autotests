package automationtests.utils.atc.services;

import com.codeborne.selenide.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static automationtests.utils.atc.services.configs.ConfigLoader.load;

public abstract class BaseService {

    static {
        applySelenideConfig();
    }

    public static final ThreadLocal<Logger> LOGGER_THREAD_LOCAL =
        new ThreadLocal<Logger>(){
            @Override
            public Logger initialValue(){
                return LogManager.getLogger("DEFAULT LOGGER");
            }
        };

    public static Logger log(){
        return LOGGER_THREAD_LOCAL.get();
    }

    public static Logger log(String loggerName){
        LOGGER_THREAD_LOCAL.set(LogManager.getLogger(loggerName));
        return LOGGER_THREAD_LOCAL.get();
    }

    private static void applySelenideConfig(){
        Configuration.screenshots = load().selenideScreenshots();
        Configuration.savePageSource = load().selenideSavePageSource();
    }

}
