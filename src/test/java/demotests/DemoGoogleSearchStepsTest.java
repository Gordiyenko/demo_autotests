package demotests;


import automationtests.steps.GooglePageSteps;
import automationtests.utils.atc.ATCoreTest;
import com.codeborne.selenide.WebDriverRunner;
import com.google.inject.Inject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import static automationtests.utils.atc.utils.internal.WebDriverFactory.chrome;

/*This class very similar to DemoGoogleSearchTest except one additional layer - step. This layer is using for
implementation more complex logic of UI interaction and more human report representation (in report all steps will be
as usual text, not as java methods).*/
@Guice
public class DemoGoogleSearchStepsTest extends ATCoreTest {

    @Inject
    GooglePageSteps step;

    @BeforeClass
    public void init(){
        WebDriverRunner.setWebDriver(chrome());
    }

    @Test
    public void demoGoogleSearchTestWithSteps(){
        step
            .openGoogleMainPage()
            .doSearch("hellow google")
            .checkResultStatus("sec");
    }

}
