package automationtests.utils.atc.services.configs;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources("classpath:internal.properties")
public interface Configuration extends Config {

    @Key("core.screen.shot")
    @DefaultValue("ALWAYS")
    String coreScreenShot();

    @Key("core.web.driver.timeout")
    @DefaultValue("5000")
    Integer coreWebDriverTimeout();

    @Key("env")
    String env();

    @Key("use.google.doc")
    @DefaultValue("false")
    Boolean useGoogleDoc();

    @Key("selenide.screenshots")
    @DefaultValue("false")
    Boolean selenideScreenshots();

    @Key("selenide.savePageSource")
    @DefaultValue("false")
    Boolean selenideSavePageSource();

    @Key("basic.auth.login")
    String basicAuthLogin();

    @Key("basic.auth.pass")
    String basicAuthPass();

    @Key("selenoid.hub.url")
    @DefaultValue("http://127.0.0.1:4444/wd/hub")
    String selenoidHubUrl();

}
